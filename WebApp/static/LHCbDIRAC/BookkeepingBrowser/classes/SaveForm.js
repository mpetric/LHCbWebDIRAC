/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
Ext.define("LHCbDIRAC.BookkeepingBrowser.classes.SaveForm", {
  extend: "Ext.window.Window",
  requires: ["Ext.util.*", "LHCbDIRAC.BookkeepingBrowser.classes.AdvancedSaveWindow"],
  alias: "widget.saveForm",

  plain: true,
  resizable: false,
  modal: true,
  constructor: function(config) {
    var me = this;
    me.callParent(arguments);
  },
  initComponent: function() {
    var me = this;

    me.form = Ext.create("widget.form", {
      bodyPadding: "12 10 10",
      border: false,
      unstyled: true,
      items: [
        {
          xtype: "fieldset",
          name: "fieldsetFiletypes",
          title: "File Type",
          autoHeight: true,
          defaultType: "radiogroup",
          items: [
            {
              xtype: "radiogroup",
              columns: 1,
              vertical: true,
              items: [
                {
                  boxLabel: "Save as a text file (*.txt)",
                  name: "rb",
                  inputValue: "1",
                  checked: true
                },
                {
                  boxLabel: "Save as a python file (*.py)",
                  name: "rb",
                  inputValue: "2"
                },
                {
                  boxLabel: "Save as a CSV file (*.csv)",
                  name: "rb",
                  inputValue: "3"
                }
              ]
            }
          ]
        },
        {
          xtype: "fieldset",
          name: "fieldsetRecords",
          title: "Records",
          defaultType: "radiogroup",
          layout: "anchor",
          defaults: {
            anchor: "100%"
          },
          items: [
            {
              xtype: "fieldcontainer",
              layout: "hbox",
              name: "fieldsetRecordsContainer",
              combineErrors: true,
              defaultType: "radiogroup",
              vertical: true,
              // columns
              // : 1,
              items: [
                {
                  xtype: "radiofield",
                  boxLabel: "All",
                  name: "records",
                  inputValue: "4",
                  checked: true,
                  flex: 1
                },
                {
                  xtype: "radiofield",
                  boxLabel: "Records",
                  name: "records",
                  inputValue: "5",
                  flex: 2,
                  listeners: {
                    change: function(newValue, oldValue, eOpts) {
                      me.onChange();
                    }
                  }
                },
                {
                  xtype: "textfield",
                  name: "from",
                  fieldLabel: "From",
                  flex: 2,
                  allowBlank: false,
                  disabled: true
                },
                {
                  xtype: "textfield",
                  name: "to",
                  fieldLabel: "To",
                  flex: 2,
                  margins: "0 0 0 4",
                  allowBlank: false,
                  disabled: true
                }
              ]
            }
          ]
        }
      ]
    });

    Ext.apply(me, {
      // width : 500,
      // height : 350,
      width: 500,
      height: 350,
      title: "Save Dialog",
      layout: "fit",
      items: me.form,
      buttons: [
        {
          text: "Save",
          handler: function() {
            me.onSave();
          }
        },
        {
          text: "Advanced Save",
          handler: function() {
            me.onAdvancedSave();
          }
        },
        {
          text: "Cancel",
          handler: function() {
            me.onCancel();
          }
        },
        {
          text: "Info",
          handler: function() {
            me.onInfo();
          }
        }
      ]
    });
    me.callParent(arguments);
    var path = "";
    for (var i = 1; i < me.scope.fullpath.length; i++) {
      if (me.scope.fullpath[i] == "/") {
        path += "_";
      } else if (me.scope.fullpath[i] == "," || me.scope.fullpath[i] == "-" || me.scope.fullpath[i] == "") {
        continue;
      } else {
        path += me.scope.fullpath[i];
      }
    }

    me.form.add({
      xtype: "fieldset",
      name: "saveAs",
      title: "Save As...",
      autoHeight: true,
      defaultType: "textfield",
      items: [
        {
          labelAlign: "top",
          anchor: "90%",
          name: "fname",
          readOnly: false,
          value: path
        }
      ]
    });
  },
  onChange: function() {
    var me = this;
    if (me.form != null) {
      var recordsFieldIndex = me.form.items.findIndex("name", "fieldsetRecords");
      if (recordsFieldIndex > -1) {
        var fieldrecord = me.form.items.getAt(recordsFieldIndex);
        var fieldrecordcontainerIndex = fieldrecord.items.findIndex("name", "fieldsetRecordsContainer");
        if (fieldrecordcontainerIndex > -1) {
          var fieldContainer = fieldrecord.items.getAt(fieldrecordcontainerIndex);
          var fromIndex = fieldContainer.items.findIndex("name", "from");
          if (fromIndex > -1) {
            var fromTextBox = fieldContainer.items.getAt(fromIndex);
            if (fromTextBox.isDisabled()) {
              fromTextBox.setDisabled(false);
            } else {
              fromTextBox.setDisabled(true);
            }
          }
          var toIndex = fieldContainer.items.findIndex("name", "to");
          if (toIndex > -1) {
            var toTextBox = fieldContainer.items.getAt(toIndex);
            if (toTextBox.isDisabled()) {
              toTextBox.setDisabled(false);
            } else {
              toTextBox.setDisabled(true);
            }
          }
        }
      }
    }
  },
  onCancel: function() {
    var me = this;
    me.doClose(); // me is the window
  },
  onInfo: function() {
    var me = this;
    var datastore = new Ext.data.JsonStore({
      autoLoad: true,

      proxy: {
        type: "ajax",
        url: "BookkeepingBrowser/getStatistics",
        reader: {
          type: "json",
          rootProperty: "result",
          idProperty: "name"
        }
      },

      fields: [
        {
          name: "nbfiles"
        },
        {
          name: "nbevents"
        },
        {
          name: "fsize"
        }
      ]
    });

    datastore.proxy.extraParams = me.__createExtraParams();

    var tpl = new Ext.XTemplate(
      '<tpl for=".">',
      '<div style="margin-bottom: 10px;" class="dataset-statistics">',
      "<br/>Number of files:<span>{nbfiles}</span>",
      "<br/>Number of events:<span>{nbevents}</span>",
      "<br/>File Size:<span>{fsize}</span>",
      "</div>",
      "</tpl>"
    );
    var panel = new Ext.Panel({
      items: new Ext.view.View({
        store: datastore,
        tpl: tpl,
        itemSelector: "div.dataset-statistics",
        autoHeight: true
      }),
      bodyStyle: "padding: 5px"
    });
    // TODO use the proper method...
    var window = Ext.create("Ext.window.Window", {
      plain: true,
      resizable: false,
      modal: false,
      width: 300,
      height: 120
    });
    window.add(panel);
    window.show();
  },
  onSave: function() {
    var me = this;
    var fileTypeIndex = me.form.items.findIndex("name", "fieldsetFiletypes");
    var fileType = me.form.items.getAt(fileTypeIndex);
    var radiobuttonindex = fileType.items.findIndex("xtype", "radiogroup");
    var radiobutton = fileType.items.getAt(radiobuttonindex);
    var txtindex = radiobutton.items.findIndex("inputValue", "1");
    var format = null;
    if (radiobutton.items.getAt(radiobutton.items.findIndex("inputValue", "1")).getValue()) {
      format = "txt";
    } else if (radiobutton.items.getAt(radiobutton.items.findIndex("inputValue", "2")).getValue()) {
      format = "py";
    } else if (radiobutton.items.getAt(radiobutton.items.findIndex("inputValue", "3")).getValue()) {
      format = "csv";
    }
    var saveAsFiledSetIndex = me.form.items.findIndex("name", "saveAs");
    var saveAsFieldSet = me.form.items.getAt(saveAsFiledSetIndex);
    var saveAsIndex = saveAsFieldSet.items.findIndex("name", "fname");
    var saveAs = saveAsFieldSet.items.getAt(saveAsIndex);
    var filename = saveAs.getValue();

    var params = me.__createExtraParams();
    params.format = format;
    params.fileName = filename;
    var bkQuery = me.bkQuery;
    if (params["TCK"].length > 0) {
      bkQuery["TCK"] = params["TCK"];
    } else {
      delete bkQuery["TCK"];
    }
    params.bkQuery = Ext.JSON.encode(bkQuery);

    var handleException = function(response, options) {
      var me = this,
        result = Ext.decode(response.responseText, true);
      var blob = new Blob([response.responseText], {
        type: "text/plain;charset=utf-8"
      });
      _global.saveAs(blob, filename);
      if (result) {
        Ext.Msg.alert("Message", result["message"]);
      }
    };

    Ext.Ajax.request({
      url: "BookkeepingBrowser/saveDataSet",
      params: params,
      // form : downloadForm,
      isUpload: true,
      scope: me,
      success: handleException,
      failure: handleException
    });
  },
  __createExtraParams: function() {
    var me = this;
    var extraParams = me.scope.__getSelectedData();
    extraParams["fullpath"] = me.scope.fullpath;
    extraParams.limit = 0;
    extraParams.start = 0;
    var recordsFieldIndex = me.form.items.findIndex("name", "fieldsetRecords");
    if (recordsFieldIndex > -1) {
      var fieldrecord = me.form.items.getAt(recordsFieldIndex);
      var fieldrecordcontainerIndex = fieldrecord.items.findIndex("name", "fieldsetRecordsContainer");
      if (fieldrecordcontainerIndex > -1) {
        var fieldContainer = fieldrecord.items.getAt(fieldrecordcontainerIndex);
        var allRecordsIndex = fieldContainer.items.findIndex("inputValue", "4");
        if (allRecordsIndex > -1) {
          var allradioButton = fieldContainer.items.getAt(allRecordsIndex);
          if (allradioButton && allradioButton.getValue()) {
            extraParams.limit = me.scope.grid
              .getStore()
              .getProxy()
              .getReader().rawData.total;
          } else {
            var fromIndex = fieldContainer.items.findIndex("name", "from");
            if (fromIndex > -1) {
              var fromTextBox = fieldContainer.items.getAt(fromIndex);
            }
            var toIndex = fieldContainer.items.findIndex("name", "to");
            if (toIndex > -1) {
              var toTextBox = fieldContainer.items.getAt(toIndex);
            }
            extraParams.start = fromTextBox.getValue();
            extraParams.limit = toTextBox.getValue();
          }
        }
      }
    }
    return extraParams;
  },
  onAdvancedSave: function() {
    var me = this;
    var advSave = Ext.create("LHCbDIRAC.BookkeepingBrowser.classes.AdvancedSaveWindow", {
      applicationName: me.scope.applicationName
    });
    advSave.show();
    advSave.on("okPressed", function() {
      var values = advSave.getValues();
      if (values) {
        advSave.onCancel();
        me.setLoading("Creating pool xml catalog...");
        var saveAsFiledSetIndex = me.form.items.findIndex("name", "saveAs");
        var saveAsFieldSet = me.form.items.getAt(saveAsFiledSetIndex);
        var saveAsIndex = saveAsFieldSet.items.findIndex("name", "fname");
        var saveAs = saveAsFieldSet.items.getAt(saveAsIndex);
        var filename = saveAs.getValue();

        var params = me.__createExtraParams();
        params.fileName = filename;
        var bkQuery = me.bkQuery;
        if (params["TCK"].length > 0) {
          bkQuery["TCK"] = params["TCK"];
        } else {
          delete bkQuery["TCK"];
        }
        params.bkQuery = Ext.JSON.encode(bkQuery);
        params.formatType = values.formatType;
        params.SiteName = values.SiteName;

        var handleException = function(response, options) {
          var me = this,
            result = Ext.decode(response.responseText, true);
          me.setLoading(false);
          if (result) {
            Ext.Msg.alert("Message", result["error"]);
            return;
          }
          var blob = new Blob([response.responseText], {
            type: "application/gzip"
          });
          _global.saveAs(blob, filename + ".tar.gz");
        };

        Ext.Ajax.request({
          url: "BookkeepingBrowser/createCatalog",
          params: params,
          isUpload: true,
          scope: me,
          success: handleException,
          failure: handleException
        });
      }
    });
  }
});
